package fos

object Infer {

  case class TypeScheme(params: List[TypeVar], tp: Type) {

    def new_inst():TypeVar = {
      inst_num += 1
      TypeVar("N" + inst_num)
    }

    def new_inst(ty1:Type, map:Map[TypeVar,TypeVar]):Type = ty1 match {
      case FunType(t1,t2) => FunType(new_inst(t1,map), new_inst(t2,map))
      case key@TypeVar(_) if map.contains(key) => map(key)
      case _ => ty1
    }

    def init:Type = {
      if (params.isEmpty) 
        tp
      else 
        new_inst(tp, params.map{x => x -> new_inst()}.toMap)
    }
  }

  var inst_num = 0
  var num = 0
  def new_type():TypeVar = {
    num += 1
    TypeVar("T" + num)
  }
  
  
  type Env = List[(String, TypeScheme)]
  type Constraint = (Type, Type)

  case class TypeError(msg: String) extends Exception(msg)
  case class UnificationError(msg: String) extends Exception(msg)

  def collect(env: Env, t: Term): (Type, List[Constraint]) = t match {
    case True() => (BoolType,Nil)
    case Zero() => (NatType,Nil)
    case False() => (BoolType,Nil)

    case IsZero(t1) => 
      val (ty, const) = collect(env, t1)
      (BoolType, (ty, NatType) :: const)

    case Succ(t1) =>
      val (ty,const) = collect(env, t1)
      (NatType, (ty, NatType) :: const)

    case Pred(t1) =>
      val (ty,const) = collect(env, t1)
      (NatType, (ty, NatType) :: const)

    case If(t1,t2,t3) => 
      val (ty1,const1) = collect(env, t1)
      val (ty2,const2) = collect(env, t2)
      val (ty3,const3) = collect(env, t3)
      (ty2, (ty1, BoolType) :: (ty2, ty3) :: const1 ++: const2 ++: const3)

    case Var(t1) if env.exists(_._1 == t1) => (env.find(_._1 == t1).get._2.init, Nil)

    case Abs(t1,tp,t2) => 
      val type1 = tp match {
        case EmptyTypeTree() => new_type()
        case _=> tp.tpe
      }
      val (ty,const) = collect((t1,TypeScheme(Nil,type1)) :: env,t2)
      (FunType(type1,ty),const)

    case App(t1,t2) => 
      val type1 = new_type()
      val (ty1,const1) = collect(env, t1)
      val (ty2,const2) = collect(env, t2)
      (type1, const1 ++: const2 :+ (ty1, FunType(ty2,type1)))

    case Let(x,ty,v,t1) =>
      val (ty1, const) = collect(env,v)
      val fun = unify(const)
      val T = fun(ty1)
      val newEnv = env.map(p => (p._1, TypeScheme(p._2.params, fun(p._2.tp))))
      val (ty2,const2) = collect(newEnv :+ (x,TypeScheme(type_var(T,newEnv).distinct,T)), t1)
      ty match {
        case EmptyTypeTree() => (ty2,const ++: const2)
        case _=> (ty2,const ++: const2 :+ (ty1,ty.tpe))
      }

    case _ => throw TypeError("Type Error " + t)
  }

  def type_var(ty: Type, env: Env): List[TypeVar] = ty match {
    case x@TypeVar(_) if !env.exists(s => is_in(s._2.tp, x)) => List(x)
    case FunType(a, b) => type_var(a,env) ::: type_var(b,env)
    case _ => Nil
  }

  def unify(cs: List[Constraint]): Type => Type = {

    if (cs.isEmpty){
      return ty => ty
    }

    cs.head match {
      case (t1, t2) if t1==t2 => unify(cs.tail)
      
      case (t1@TypeVar(_), t2) if !is_in(t2,t1) =>
        unify(
          cs.tail.map{
            x => (subst(x._1, t1, t2), subst(x._2, t1, t2))}
        ).compose{
          ty => subst(ty, t1, t2)
        }
      
      case (t1, t2@TypeVar(_)) if !is_in(t1,t2) =>
        unify(
          cs.tail.map{
            x => (subst(x._1, t2, t1), subst(x._2, t2, t1))
          }
        ).compose{
          ty => subst(ty, t2, t1)
        }
      
      case (FunType(s1,s2),FunType(t1,t2)) =>
        unify((s2,t2) :: (s1,t1) :: cs.tail)
      
      case _=> throw TypeError("Error when unifying")
    }

  }

  def is_in(ty:Type, testType:Type):Boolean = ty match {
    case t1 if t1 == testType => true
    case FunType(t1,t2) => is_in(t1, testType) || is_in(t2, testType)
    case _ => false
  }
  
  def subst(x:Type, old_term:Type, new_term:Type):Type = {
    if (x == old_term)
      return new_term
    x match {
      case FunType(x1,x2) => 
        FunType(subst(x1, old_term, new_term), subst(x2, old_term, new_term))
      case _=> x
    }
  }
}
