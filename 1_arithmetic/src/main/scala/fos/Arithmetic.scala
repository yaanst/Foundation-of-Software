package fos

import scala.util.parsing.combinator.syntactical.StandardTokenParsers
import scala.util.parsing.input._
import scala.util.parsing.combinator.Parsers

/** This object implements a parser and evaluator for the NB
 *  language of booleans and numbers found in Chapter 3 of
 *  the TAPL book.
 */
object Arithmetic extends StandardTokenParsers {
  lexical.reserved ++= List("true", "false", "0", "if", "then", "else", "succ", "pred", "iszero")

  import lexical.NumericLit

  /** term ::= 'true'
             | 'false'
             | 'if' term 'then' term 'else' term
             | '0'
             | 'succ' term
             | 'pred' term
             | 'iszero' term
   */

  // Convert a numericLit into Succ(...Succ(Zero))
  def numlitToTerm(count: Int): Term =
    if (count > 0)
      Succ(numlitToTerm(count - 1))
    else
      Zero

  // Check if a Term is a numeric value (Zero or Succ(...Succ(Zero))
  //TODO: need to set 'e' as of type either Termisstuck or NoReductionPossible only (so it can take an arg)
  def isNV(x: Term): Boolean  =
    x match {
      case Zero => true
      case Pred(y) =>
        if (isNV(y))
          true
        else
          false
      case Succ(y) => isNV(y)
      case If(cond, exp1, exp2) => eval(cond) match {
          case True => isNV(exp1)
          case False => isNV(exp2)
          case _ => false
      }
      case _ => false
    }

  def term: Parser[Term] =
    numericLit ^^ (x => numlitToTerm(x.toInt)) |
    "true" ^^^ True |
    "false" ^^^ False |
    ("succ" ~ term) ^^ (args => Succ(args._2)) |
    ("pred" ~ term) ^^ (args => Pred(args._2)) |
    ("iszero" ~ term) ^^ (args => IsZero(args._2)) |
    ("if" ~ term ~ "then" ~ term ~ "else" ~ term) ^^ {
      case _ ~ cond ~ _ ~ exp1 ~ _ ~ exp2 => If(cond, exp1, exp2)
    }


  case class NoReductionPossible(t: Term) extends Exception(t.toString)

  /** Return a list of at most n terms, each being one step of reduction. */
  def path(t: Term, n: Int = 64): List[Term] =
    if (n <= 0) Nil
    else
      t :: {
        try {
          path(reduce(t), n - 1)
        } catch {
          case NoReductionPossible(t1) =>
            Nil
        }
      }

  /** Perform one step of reduction, when possible.
   *  If reduction is not possible NoReductionPossible exception
   *  with corresponding irreducible term should be thrown.
   */
  def reduce(t: Term): Term = t match {
    case IsZero(x) =>
      x match {
        case Zero => True
        case Succ(y) =>
          if (isNV(y))
            False
          else
            throw NoReductionPossible(y)
        case _ => IsZero(reduce(x))
      }

    case Pred(x) =>
      x match {
        case Zero => Zero
        case Succ(y) =>
          if (isNV(y))
            y
          else
            throw NoReductionPossible(y)
        case _ => Pred(reduce(x))
      }

    case Succ(x) => Succ(reduce(x))

    case If(cond, exp1, exp2) =>
      cond match {
        case True => exp1
        case False => exp2
        case _ => If(reduce(cond), exp1, exp2)
      }

    case _ => throw NoReductionPossible(t)
  }

  case class TermIsStuck(t: Term) extends Exception(t.toString)

  /** Perform big step evaluation (result is always a value.)
   *  If evaluation is not possible TermIsStuck exception with
   *  corresponding inner irreducible term should be thrown.
   */
  def eval(t: Term): Term = t match {
    case Zero | True | False => t

    case If(cond, exp1, exp2) => eval(cond) match {
      case True => eval(exp1)
      case False => eval(exp2)
      case _ => throw TermIsStuck(cond)
    }

    case Succ(x) => x match {
      case y =>
        if (isNV(y))
          Succ(eval(y))
        else
          throw TermIsStuck(y)
      case _ => throw TermIsStuck(x)
    }


    case Pred(x) => eval(x) match {
      case Zero => Zero
      case Succ(y) =>
        if (isNV(y))
          eval(y)
        else
          throw TermIsStuck(y)
    }

    case IsZero(x) => eval(x) match {
      case Zero => True
      case Succ(y) =>
        if (isNV(y))
          False
        else
          throw TermIsStuck(y)
      case _ => throw TermIsStuck(x)
    }

    case _ => throw TermIsStuck(t)
  }


  def main(args: Array[String]): Unit = {
    val stdin = new java.io.BufferedReader(new java.io.InputStreamReader(System.in))
    val tokens = new lexical.Scanner(stdin.readLine())
    phrase(term)(tokens) match {
      case Success(trees, _) =>
        for (t <- path(trees))
          println(t)
        try {
          print("Big step: ")
          println(eval(trees))
        } catch {
          case TermIsStuck(t) => println("Stuck term: " + t)
        }
      case e =>
        println(e)
    }
  }
}