package fos

import sun.awt.AWTIcon32_java_icon16_png

import scala.util.parsing.combinator.syntactical.StandardTokenParsers
import scala.util.parsing.input._

/** This object implements a parser and evaluator for the
 *  untyped lambda calculus found in Chapter 5 of
 *  the TAPL book.
 */
object Untyped extends StandardTokenParsers {
  lexical.delimiters ++= List("(", ")", "\\", ".")
  import lexical.Identifier

  /** t ::= x
          | '\' x '.' t
          | t t
          | '(' t ')'
   */

  // Need to use chainl1, (+ need to create a "reduce" function ???)
  // or use foldLeft
  def expr: Parser[Term] =
    ident ^^ Var |
    ("\\" ~> ident) ~ ("." ~> term) ^^ {
      case v ~ t => Abs(v, t)
    } |
    "(" ~> term <~ ")"

  def term: Parser[Term] =
      expr ~ rep(expr) ^^  {
        case t ~ ts => ts.foldLeft(t){(t1, t2) => App(t1, t2)}
      }


  /** <p>
   *    Alpha conversion: term <code>t</code> should be a lambda abstraction
   *    <code>\x. t</code>.
   *  </p>
   *  <p>
   *    All free occurences of <code>x</code> inside term <code>t/code>
   *    will be renamed to a unique name.
   *  </p>
   *
   *  @param t the given lambda abstraction.
   *  @return  the transformed term with bound variables renamed.
   */
  def alpha(t: Term): Term = (
    t match {
      case Abs(x, t1) => {
        val countLabel = increment().toString()
        Abs(countLabel, assignLabel(t1, x, countLabel))
      }
      case _ => t
    }
  )

   def assignLabel(t: Term, x: String, countLabel: String) : Term = (
     t match {
       case Var(`x`) => Var(countLabel)
       case Var(other) if other!=x => t
       case Abs(`x`, t1) => Abs(countLabel, assignLabel(t1, x, countLabel))
       case Abs(other, t1) => Abs(other, assignLabel(t1, x, countLabel))
       case App(t1, t2) => App(assignLabel(t1, x, countLabel), assignLabel(t2, x, countLabel))
       case _ => t
     }
   )

  var counter : Long = 0
  def increment(): Long = {
    counter += 1
    counter
  }


  /** Straight forward substitution method
   *  (see definition 5.3.5 in TAPL book).
   *  [x -> s]t
   *
   *  @param t the term in which we perform substitution
   *  @param x the variable name
   *  @param s the term we replace x with
   *  @return  ...
   */
  def subst(t: Term, x: String, s: Term): Term = (
    t match {
      case Var(`x`) => s
      case Var(other) if other!=x => t
      case Abs(`x`, t1) => t
      case Abs(other, t1) if other!=x && !(freeVar(s) contains other) => Abs(other, subst(t1, x, s))
      case Abs(other, t1) if other!=x && (freeVar(s) contains other) => subst(alpha(t), x, s)
      case App(t1, t2) => App(subst(t1, x, s), subst(t2, x, s))
      case _ => t
    }
  )


  def freeVar(t:Term): List[String] = {
    val freeList=List()
    t match{
      case Var(x) => x +: freeList
      case Abs(x,t) => freeVar(t) diff List(x)
      case App(t1,t2) => freeVar(t1) ++: freeVar(t2)
    }
  }

  /** Term 't' does not match any reduction rule. */
  case class NoReductionPossible(t: Term) extends Exception(t.toString)

  /** Normal order (leftmost, outermost redex first).
   *
   *  @param t the initial term
   *  @return  the reduced term
   */
  def reduceNormalOrder(t: Term): Term = t match {
      case App(Abs(x, t1), t2) => subst(t1, x, t2)
      case App(t1, t2) => t1 match {
        case Var(_) => App(t1, reduceNormalOrder(t2))
        case App(_,_) =>
          try {
            App(reduceNormalOrder(t1), t2)
          }
          catch {
            case NoReductionPossible(e) => App(t1, reduceNormalOrder(t2))
          }
        case _ => throw NoReductionPossible(t)
        }
      case Abs(x, t1) => Abs(x, reduceNormalOrder(t1))
      case _ => throw NoReductionPossible(t)
    }


  /** Call by value reducer. */
  def reduceCallByValue(t: Term): Term = t match {
    case App(Abs(x, t1), t2) if t2.isInstanceOf[Abs] => subst(t1, x, t2)
    case App(t1, t2) => t1 match {
      case App(_, _) => App(reduceCallByValue(t1), t2)
      case Abs(_, _) if t2.isInstanceOf[App] => App(t1, reduceCallByValue(t2))
      case _ => throw NoReductionPossible(t)
    }
    case _ => throw NoReductionPossible(t)
  }

  /** Returns a stream of terms, each being one step of reduction.
   *
   *  @param t      the initial term
   *  @param reduce the method that reduces a term by one step.
   *  @return       the stream of terms representing the big reduction.
   */
  def path(t: Term, reduce: Term => Term): Stream[Term] =
    try {
      var t1 = reduce(t)
      Stream.cons(t, path(t1, reduce))
    } catch {
      case NoReductionPossible(_) =>
        Stream.cons(t, Stream.empty)
    }

  def main(args: Array[String]): Unit = {
    val stdin = new java.io.BufferedReader(new java.io.InputStreamReader(System.in))
    val tokens = new lexical.Scanner(stdin.readLine())
    phrase(term)(tokens) match {
      case Success(trees, _) =>
        println("normal order: ")
        for (t <- path(trees, reduceNormalOrder))
          println(t)
        println("call-by-value: ")
        for (t <- path(trees, reduceCallByValue))
          println(t)
      case e =>
        println(e)
    }
  }
}
