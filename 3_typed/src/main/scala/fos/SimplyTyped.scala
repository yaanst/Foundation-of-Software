package fos

import scala.util.parsing.combinator.syntactical.StandardTokenParsers
import scala.util.parsing.input._

/** This object implements a parser and evaluator for the
 *  simply typed lambda calculus found in Chapter 9 of
 *  the TAPL book.
 */
object SimplyTyped extends StandardTokenParsers {
  lexical.delimiters ++= List("(", ")", "\\", ".", ":", "=", "->", "{", "}", ",", "*")
  lexical.reserved   ++= List("Bool", "Nat", "true", "false", "if", "then", "else", "succ",
                              "pred", "iszero", "let", "in", "fst", "snd")


  // Functions to parse simple types
  def repType =
  "Bool" ^^^ TypeBool |
  "Nat" ^^^ TypeNat |
  "(" ~> parseType <~ ")"

  def pairType =
    rep1sep(repType, "*") ^^ {
      _.reduceRight(TypePair(_, _))
    }

  def parseType: Parser[Type] =
    rep1sep(pairType, "->") ^^ {
      _.reduceRight(TypeFun(_,_))
    }


  // Functions to parse Terms
  def parseNumeric(i: Int): Term =
    i match {
      case 0 => Zero()
      case x => Succ(parseNumeric(x - 1))
    }

  def repTerm: Parser[Term] =
  "true" ^^^ True() |
  "false" ^^^ False() |
   "if" ~ Term ~ "then" ~ Term ~ "else" ~ Term ^^ {
     case _~cond~_~t1~_~t2 => If(cond,t1,t2)
  } |
  numericLit ^^ {x => parseNumeric(x.toInt)} |
  "succ" ~> Term ^^ Succ |
  "pred" ~> Term ^^ Pred |
  "iszero" ~> Term ^^ IsZero |
  "fst" ~> Term ^^ First |
  "snd" ~> Term ^^ Second |
  ident ^^ Var |
  "(" ~> Term <~ ")" |
  "{" ~ Term ~ "," ~ Term ~ "}" ^^ {
    case _ ~ t1 ~ _ ~ t2 ~ _ => TermPair(t1, t2)
  } |
  "\\" ~ ident ~ ":" ~ parseType ~ "." ~ Term ^^ {
    case _~i~_~ty~_~t => Abs(i,ty,t)
  }|
  "let" ~ ident ~ ":" ~ parseType ~ "=" ~ Term ~ "in" ~ Term ^^ {
    case _~i~_~ty~_~t1~_~t2 => App(Abs(i, ty, t2), t1)
  }

  /** Term     ::= SimpleTerm { SimpleTerm }
    */
  def Term: Parser[Term] =
    repTerm ~ rep(repTerm) ^^ {
      case t ~ ts => ts.foldLeft(t){(t1,t2) => App(t1,t2)}
    }


  /** Thrown when no reduction rule applies to the given term. */
  case class NoRuleApplies(t: Term) extends Exception(t.toString)

  /** Print an error message, together with the position where it occured. */
  case class TypeError(t: Term, msg: String) extends Exception(msg) {
    override def toString =
      msg + "\n" + t
  }

  /** The context is a list of variable names paired with their type. */
  type Context = List[(String, Type)]

  def isNat(t: Term): Boolean = t match {
    case Zero() => true
    case Succ(x) => isNat(x)
    case _ => false
  }
  def isVal(t: Term): Boolean = t match {
    case True() | False() | Abs(_,_,_) => true
    case TermPair(t1,t2) => isVal(t1) && isVal(t2)
    case _ => isNat(t)
  }

  /** Call by value reducer. */
  def reduce(t: Term): Term = t match {
    case If(True(),t1,_) => t1
    case If(False(),_,t2) => t2
    case IsZero(Zero()) => True()
    case IsZero(Succ(t1)) if isNat(t1) => False()
    case Pred(Zero()) => Zero()
    case Pred(Succ(t1)) if isNat(t1) => t1
    case App(Abs(x,_,t1),t2) if isVal(t2) => subst(t1,x,t2)

    case If(cond,t1,t2) => If(reduce(cond),t1,t2)
    case IsZero(t1) => IsZero(reduce(t1))
    case Succ(t1) => Succ(reduce(t1))
    case Pred(t1) => Pred(reduce(t1))

    case App(t1,t2) if !isVal(t1) => App(reduce(t1),t2)
    case App(t1,t2) if isVal(t1) && !isVal(t2) => App(t1,reduce(t2))

    case First(TermPair(t1,t2)) if isVal(t1)&&isVal(t2) => t1
    case Second(TermPair(t1,t2)) if isVal(t1)&&isVal(t2) => t2
    case First(t1) => First(reduce(t1))
    case Second(t2) => Second(reduce(t2))
    case TermPair(t1,t2) if !isVal(t1) => TermPair(reduce(t1),t2)
    case TermPair(t1,t2) if isVal(t1) && !isVal(t2) => TermPair(t1,reduce(t2))
    case _ =>throw NoRuleApplies(t)
  }


  def freeVar(t: Term): Set[String] = t match {
      case Var(x) => Set(x)
      case Abs(s, _, t1) => freeVar(t1) - s
      case App(t1, t2) => freeVar(t1) ++ freeVar(t2)
      case Pred(t1) => freeVar(t1)
      case Succ(t1) => freeVar(t1)
      case IsZero(t1) => freeVar(t1)
      case If(cond, t1, t2) => freeVar(cond) ++ freeVar(t1) ++ freeVar(t2)
      case TermPair(t1, t2) => freeVar(t1) ++ freeVar(t2)
      case First(t1) => freeVar(t1)
      case Second(t1) => freeVar(t1)
      case _ => Set.empty
    }

  def rename(t:Term, s:String):Term = t match{
    case Var(`s`) => Var(s + "renamed")
    case Abs(y,ty1,t1) if y!=s => Abs(y,ty1,rename(t1,s))
    case App(t1,t2) => App(rename(t1,s),rename(t2,s))
    case Succ(t1) => Succ(rename(t1,s))
    case Pred(t1) => Pred(rename(t1,s))
    case IsZero(t1) => IsZero(rename(t1,s))
    case If(cons,t1,t2) => If(rename(cons,s),rename(t1,s),rename(t2,s))
    case First(t1) => First(rename(t1,s))
    case Second(t1) => Second(rename(t1,s))
    case TermPair(t1,t2) => TermPair(rename(t1,s),rename(t2,s))
    case _=> t
  }

  /** Returns the type of the given term <code>t</code>.
    *
    *  @param t   the term in which we perform substitution
    *  @param x   the variable name
    *  @param s   the term we replace x with
    *  @return    ...
    */
  def subst(t: Term, x: String, s: Term): Term = t match {
    case Succ(t1) => Succ(subst(t1, x, s))
    case Pred(t1) => Pred(subst(t1, x, s))
    case IsZero(t1) => IsZero(subst(t1, x, s))
    case If(cond, t1, t2) => If(subst(cond,x,s), subst(t1,x,s), subst(t1,x,s))
    case First(t1) => First(subst(t1,x,s))
    case Second(t1) => Second(subst(t1,x,s))
    case TermPair(t1,t2) => TermPair(subst(t1,x,s),subst(t2,x,s))
    case Var(`x`) => s
    case Abs(y, ty, t1) if y != x && !(freeVar(s) contains y) => Abs(y, ty, subst(t1,x,s))
    case Abs(y, ty, t1) if y != x && (freeVar(s) contains y) => subst(Abs(y+"renamed",ty,rename(t1,y)),x,s)
    case App(left,right) => App(subst(left,x,s),subst(right,x,s))
    case _=> t
  }

  /** Returns the type of the given term <code>t</code>.
   *
   *  @param ctx the initial context
   *  @param t   the given term
   *  @return    the computed type
   */
  def typeof(ctx: Context, t: Term): Type = t match {
    case True() => TypeBool
    case False() => TypeBool
    case Zero() => TypeNat
    case Pred(t1) if typeof(ctx,t1) == TypeNat => TypeNat
    case Succ(t1) if typeof(ctx,t1) == TypeNat => TypeNat
    case IsZero(t1) if typeof(ctx,t1) == TypeNat => TypeBool
    case If(cond,t1,t2) if typeof(ctx,cond) == TypeBool && typeof(ctx,t1)==typeof(ctx,t2) => typeof(ctx,t1)
    case Var(x) if ctx.exists(_._1 == x)=> retrieveType(ctx,x)
    case Abs(x,ty1,t2) => {
      val ty2 = typeof((x,ty1) +: ctx, t2)
      TypeFun(ty1,ty2)
    }
    case App(t1,t2) => {
      typeof(ctx,t1) match{
        case TypeFun(ty2,ty3) if ty2 == typeof(ctx,t2) => ty3
        case TypeFun(ty2,ty3) if ty2 != typeof(ctx,t2) =>
          throw TypeError(t,"parameter type mismatch: expected "+ty2+", found "+typeof(ctx,t2))
        case _ =>
          throw TypeError(t,"parameter type mismatch: expected TypeFun, found "+typeof(ctx,t1))
      }
    }
    case TermPair(t1,t2) => {
      val ty1 = typeof(ctx,t1)
      val ty2 = typeof(ctx,t2)
      TypePair(ty1,ty2)
    }
    case First(t1) => {
      typeof(ctx,t1) match{
        case TypePair(ty1,ty2) => ty1
        case _=>
          throw TypeError(t,"pair type expected but "+typeof(ctx,t1)+" found");
      }
    }
    case Second(t1) => {
      typeof(ctx,t1) match{
        case TypePair(ty1,ty2) => ty2
        case _=>
          throw TypeError(t,"pair type expected but "+typeof(ctx,t1)+" found");
      }
    }
    case _=> throw TypeError(t,"parameter type mismatch")
  }

      def retrieveType(ctx: Context, s: String):Type = {
        ctx.find(_._1 == s).get._2
      }


  /** Returns a stream of terms, each being one step of reduction.
   *
   *  @param t      the initial term
   *  @param reduce the evaluation strategy used for reduction.
   *  @return       the stream of terms representing the big reduction.
   */
  def path(t: Term, reduce: Term => Term): Stream[Term] =
    try {
      var t1 = reduce(t)
      Stream.cons(t, path(t1, reduce))
    } catch {
      case NoRuleApplies(_) =>
        Stream.cons(t, Stream.empty)
    }

  def main(args: Array[String]): Unit = {
    val stdin = new java.io.BufferedReader(new java.io.InputStreamReader(System.in))
    val tokens = new lexical.Scanner(stdin.readLine())
    phrase(Term)(tokens) match {
      case Success(trees, _) =>
        try {
          println("typed: " + typeof(Nil, trees))
          for (t <- path(trees, reduce))
            println(t)
        } catch {
          case tperror: Exception => println(tperror.toString)
        }
      case e =>
        println(e)
    }
  }
}
